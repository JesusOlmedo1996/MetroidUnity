﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeSeconds : MonoBehaviour {

    private float timeSeconds = 0f;

	// Update is called once per frame
	void Update () {

        timeSeconds += Time.deltaTime;

        if (timeSeconds > 15)
        {
            SceneManager.LoadScene("MainMenu");
        }


    }
}
