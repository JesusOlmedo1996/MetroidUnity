﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyDied : MonoBehaviour {



    public float maxSpeed = 1f;
    public float speed = 1f;

    private Rigidbody2D enemy;

    // Use this for initialization
    void Start()
    {
        enemy = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        enemy.AddForce(Vector2.right * speed);
        float limitedSpeed = Mathf.Clamp(enemy.velocity.x, -maxSpeed, maxSpeed);
        enemy.velocity = new Vector2(limitedSpeed, enemy.velocity.y);

        if (enemy.velocity.x > -0.025f && enemy.velocity.x < 0.025f)
        {
            speed = -speed;
            enemy.velocity = new Vector2(speed, enemy.velocity.y);
        }

        if (speed < 0)
        {
            transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
        }
        else if (speed > 0)
        {
            transform.localScale = new Vector3(-2.5f, 2.5f, 2.5f);
        }
    }



    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Samus")
        {
            SceneManager.LoadScene("YouLose");
        }

        if (collision.gameObject.tag == "Bang")
        {
             Destroy(gameObject);
        }
        
    }

   
}
