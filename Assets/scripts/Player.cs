﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    private Rigidbody2D myRigibody;

    private Animator myAnimator;

    [SerializeField]
    private float movementSpeed;

    private bool slide;

    private bool facingLeft;

    [SerializeField]
    private Transform[] groundPoints;

    [SerializeField]
    private float groundRadius;

    [SerializeField]
    private LayerMask whatIsGround;

    private bool isGrounded;

    private bool jump;

    [SerializeField]
    private bool airControl;

    [SerializeField]
    private float jumpForce;

    [SerializeField]
    private GameObject bang;


	// Use this for initialization
	void Start ()
    {
        facingLeft = true;
        myRigibody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();

    }

    void Update()
    {
        HandleInput();
    }
    // Update is called once per frame
    void FixedUpdate ()
    {
        float horizontal = Input.GetAxis("Horizontal");

        isGrounded = IsGrounded();
        
        HandleMovement(horizontal);

        Flip(horizontal);

        HandleLayers();

        ResetValues();

    }

    private void HandleMovement(float horizontal)
    {

        if (myRigibody.velocity.y <0)
        {
            myAnimator.SetBool("land", true);
        }

        if (isGrounded || airControl)
        {
            myRigibody.velocity = new Vector2(horizontal * movementSpeed, myRigibody.velocity.y);
        }

        if (isGrounded && jump)
        {
            isGrounded = false;
            myRigibody.AddForce(new Vector2(0, jumpForce));
            myAnimator.SetTrigger("jump");
        }   

        if(slide && !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsName("samus_ball"))
        {
            myAnimator.SetBool("slide", true);
        }
        else if(!this.myAnimator.GetCurrentAnimatorStateInfo(0).IsName("samus_ball"))
        {
            myAnimator.SetBool("slide", false);
        }

        myAnimator.SetFloat("speed", Mathf.Abs(horizontal));


    }
    
    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
        }

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            slide = true;
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            Bang(0);
        }
    }

    private void Flip(float horizontal)
    {

        if (horizontal > 0 && !facingLeft || horizontal < 0 && facingLeft)
        {
            facingLeft = !facingLeft;

            Vector3 theScale = transform.localScale;

            theScale.x *= -1;

            transform.localScale = theScale;
        }

    }

    private void ResetValues()
    {
        slide = false;
        jump = false;
    }

    private bool IsGrounded()
    {
        if (myRigibody.velocity.y <= 0)
        {
            foreach (Transform point in groundPoints)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);

                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject != gameObject)
                    {
                        myAnimator.ResetTrigger("jump");
                        myAnimator.SetBool("land", false);
                        return true;
                    }
                }

            }
        }
        return false;
    }

    private void HandleLayers()
    {
        if (!isGrounded)
        {
            myAnimator.SetLayerWeight(1, 1);
        }
        else
        {
            myAnimator.SetLayerWeight(1, 0);
        }
    }

    public void Bang(int value)
    {
        if (facingLeft)
        {
            GameObject tmp = (GameObject)Instantiate(bang, transform.position, Quaternion.Euler(new Vector3(0,0,180)));
            tmp.GetComponent<Bang>().Initialize(Vector2.right);
        }
        else
        {
            GameObject tmp = (GameObject)Instantiate(bang, transform.position, Quaternion.Euler(new Vector3(0, 0, 180)));
            tmp.GetComponent<Bang>().Initialize(Vector2.left);
        }
    }

}
